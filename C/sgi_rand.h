#ifndef SGI_RAND_H
# define SGI_RAND_H

# define SGI_MAX_RAND (0b11111111111111111111111111111111)

/**
 * Initialize the RNG
 * @param seed : the seed
 */
void sgi_srand(unsigned int seed);

/**
 * Get the next pseudo-random value
 * @return the next pseudo-random value
 */
unsigned int sgi_rand(void);

/**
 * @return a pseudo-random value in [-2^31, 2^31-1]
 */
int sgi_rand_i(void);

/**
 * @return a pseudo-random value in [0, 2^32-1]
 */
unsigned int sgi_rand_u(void);

/**
 * @return a pseudo-random value in [0, 1]
 */
float sgi_rand_f(void);

/**
 * @return a pseudo-random value in [0, 1]
 */
double sgi_rand_d(void);

/**
 * @return a pseudo-random value in [a, b] generated 
 */ 
double sgi_rand_uniform(double a, double b);

/**
 * @return a pseudo-random value
 */
double sgi_rand_gauss(double a, double b);

/**
 * @param n : size of the vector
 * @param dst : the vecto
 * If NULL, it is allocated via malloc;
 * and so it should be released later using free.
 *
 * @return a vector of n double in [0, 1]
 */
double * sgi_rand_vec(unsigned int n, double * dst);

#endif
