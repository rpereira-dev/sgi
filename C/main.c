# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>
# include <unistd.h>

# include "sgi_lhs.h"
# include "sgi_rand.h"

static int help(char ** argv) {
	fprintf(stderr, "%s : error usage.\n", argv[0]);
	fprintf(stderr, "Examples:\n");
	fprintf(stderr, "    %s -r uniform -s 42 -a 0 -b 1 -n 1000\n", argv[0]);
	fprintf(stderr, "    %s -r lhs -s 42 -n 100 -k 3\n", argv[0]);
	return 1;
}

int main(int argc, char ** argv) {
	char * r = "uniform";
	unsigned int n = 100;
	unsigned int s = time(NULL);
	unsigned int k = 1;
	double a = 0.0;
	double b = 1.0;
	char opt;
	while ((opt = getopt(argc, argv, "hr:s:a:b:n:k:")) != -1) {
		switch (opt) {
			case 'h':
				return help(argv);
			case 'r':
				r = optarg;
				break ;
			case 's':
				s = (unsigned int)atoi(optarg);
				break;
			case 'a':
				a = atof(optarg);
				break;
			case 'b':
				b = atof(optarg);
				break;
			case 'n':
				n = (unsigned int)atoi(optarg);
				break;
			case 'k':
				k = (unsigned int)atoi(optarg);
				break;
		}
	}
	
	sgi_srand(s);


	unsigned int i, j;
	if (strcmp(r, "uniform") == 0) {
		for (i = 0 ; i < n ; i++) {
			printf("%lf\n", sgi_rand_uniform(a, b));
		}
	} else if (strcmp(r, "gauss") == 0) {
		for (i = 0 ; i < n ; i++) {
			printf("%lf\n", sgi_rand_gauss(a, b));
		}
	} else if (strcmp(r, "lhs") == 0) {
		double * values = sgi_lhs(n, k, NULL);
		for (i = 0 ; i < n ; i++) {
			for (j = 0 ; j < k ; j++) {
				printf("%lf", values[j * n + i]);
				if (j != k - 1) {
					printf(",");
				}
			}
			printf("\n");
		}
		free(values);
	}

	return 0;
}
