/**
 * The implementation is a Knuth & Lewis RNG
 */

# include <float.h>
# include <math.h>
# include <stdlib.h>

# include "sgi_rand.h"

typedef struct findorder_s {
	double v;
	unsigned int order;
} findorder_t;

static int findorder_compar(const void * a, const void * b) {
	findorder_t * x = (findorder_t *)a;
	findorder_t * y = (findorder_t *)b;
	return x->v < y->v ? -1 : x->v > y->v ? 1 : 0;
}

/**
 * Find the order of each vector element (zero based)
 * @param v the vector to be ordered
 * @param n size of the vector
 * @param order the order of the elements
 */
static unsigned int * findorder(double * v,
				unsigned int n,
				unsigned int * dst) {

	if (dst == NULL) {
		dst = (unsigned int *) malloc(sizeof(unsigned int) * n);
		if (dst == NULL) {
			return NULL;
		}
	}

	findorder_t * p = (findorder_t *)malloc(sizeof(findorder_t) * n);
	if (p == NULL) {
		return NULL;
	}

	unsigned int i;
	for (i = 0 ; i < n ; i++) {
		p[i].v = v[i];
		p[i].order = i;
	}

	qsort(p, n, sizeof(findorder_t), findorder_compar);

	for (i = 0 ; i < n ; i++) {
		dst[i] = (p + i)->order;
	}
	free(p);

	return dst;
}

double * sgi_lhs(unsigned int n, unsigned int k, double * dst) {
	unsigned int allocated = 0;
	if (dst == NULL) {
		dst = (double *) malloc(sizeof(double) * n * k);
		if (dst == NULL) {
			return NULL;
		}
		allocated = 1;
	}

	void * mem = malloc(sizeof(double) * n * 2 + sizeof(unsigned int) * n);

	if (mem == NULL) {
		if (allocated) {
			free(dst);
		}
		return NULL;
	}

	double * x = (double *)mem;
	double * y = x + n;
	unsigned int * order = (unsigned int *)(y + n);

	unsigned int i, j;
	for (j = 0; j < k; j++) {
		sgi_rand_vec(n, x);
		sgi_rand_vec(n, y);
		findorder(x, n, order);
		for (i = 0; i < n; i++) {
			dst[j * n + i] = (order[i] + y[i]) / (double)n;
		}
	}
	free(mem);
	return dst;
}

