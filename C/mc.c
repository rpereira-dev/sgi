# include <cstdio>
# include <cstdlib>
# include <ctime>

# include "sgi_rand.h"

# include <armadillo>
# include <mpi.h>

# define GATHER_RANK 0

double montecarlo(arma::vec & u) {
	arma::vec x = 0.8 * u + 0.1;
	return 0.8 * (double)arma::mean(
		0.5 * arma::exp(arma::asin(x)) % (x / arma::sqrt(1.0 - x % x))
	);
}

int main(int argc, char ** argv) {
	if (argc != 2) {
		fprintf(stderr, "usage: %s [N]\n", argv[0]);
		return 1;
	}

	MPI_Init(&argc, &argv);

	/* Get the rank of the process */
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	/* seed per process */
	sgi_srand((world_rank + 8191) * 131071 + time(NULL));

	/* generate U per process */
	/* vec(ptr_aux_mem, number_of_elements, copy_aux_mem = true, strict = false) */
	double r;
	{
        	unsigned int n = (unsigned int) atoi(argv[1]);
		double * ptr = sgi_rand_vec(n, NULL); 
		arma::vec u(ptr, n, false, true);
		r = montecarlo(u);
		free(ptr);
	}

	/* gather results */
	double * rs = (world_rank == GATHER_RANK) ? (double *) malloc(sizeof(double) * world_size) : NULL;
	MPI_Gather(
		&r, 1, MPI_DOUBLE,
		rs, 1, MPI_DOUBLE,
		GATHER_RANK, MPI_COMM_WORLD
	);
        if (world_rank == GATHER_RANK) {
		int i;
		for (i = 0 ; i < world_size ; i++) {
        	        printf("%f\n", rs[i]);
		}
        }

	MPI_Finalize();
	return 0;
}
