#ifndef SGI_LHS_H
# define SGI_LHS_H

/**
 * @param n : number of vectors
 * @param k : size of the vector
 * @param dst : n * k * sizeof(unsigned int) byte buffer.
 * If NULL, it is allocated via malloc;
 * and so it should be released later using free.
 *
 * @return n vector of dimension k, of double in [0, 1.0]
 */
double * sgi_lhs(unsigned int n, unsigned int k, double * dst);

#endif
