# include <mpi.h>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>

# include "sgi_rand.h"

int main(int argc, char ** argv) {
	if (argc != 2) {
		fprintf(stderr, "usage: %s [N]\n", argv[0]);
		return 1;
	}

	MPI_Init(&argc, &argv);

	/* Get the rank of the process */
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	/* seed per process */
	sgi_srand((world_rank + 8191) * 131071 + time(NULL));

	/* count point in the circle */
	unsigned int n = (unsigned int) atoi(argv[1]);
	unsigned int m = 0;
	unsigned int i;
	for (i = 0 ; i < n ; i++) {
		double x = sgi_rand_d();
		double y = sgi_rand_d();
		if (x * x + y * y < 1.0) {
			++m;
		}
	}

	unsigned int * ms = world_rank == 0 ? (unsigned int *) malloc(sizeof(unsigned int) * world_size) : NULL;
	MPI_Gather(
		&m, 1, MPI_UNSIGNED,
		ms, 1, MPI_UNSIGNED,
		0, MPI_COMM_WORLD
	);

	if (world_rank == 0) {
		unsigned int sum = 0;
		for (i = 0 ; i < (unsigned int)world_size; i++) {
			sum += ms[i];		
		}
		printf("%f\n", sum / (double)(world_size * n) * 4.0);
	}

	free(ms);
	MPI_Finalize();
	return 0;
}
